(function() {
	'use strict';

	var browserSync = require('browser-sync').create();
	var gulp = require('gulp');
	var gutil = require('gulp-util');
	var ftp = require('vinyl-ftp');
	var concat = require('gulp-concat');
	var concatCss = require('gulp-concat-css');
	var sass = require('gulp-sass');
	var uglify = require('gulp-uglify');
	var minifyCss = require('gulp-minify-css');
	var gulpif = require('gulp-if');
	var rename = require('gulp-rename');
	var gulpCopy = require('gulp-copy');
	var autoprefixer = require('gulp-autoprefixer');
	var plumber = require('gulp-plumber');

	// Configuration
	var config = {
		bower: 	'./bower_components',
		assets: './assets',
		build:	'./wp-content/themes/litmus7'
	};

	var onError = function (err) {
		gutil.beep();
		console.log(err);
	};

	gulp.task('sass', function () {
		gulp.src([
				config.assets+'/sass/theme.scss',
				config.bower+'/animate.css/animate.css',				
				config.bower+'/slick-carousel/slick/slick.css', 
				config.bower+'/magnific-popup/dist/magnific-popup.css', 
				config.assets+'/sass/litmus7.scss',
			])
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(sass())
			.pipe(autoprefixer({
					browsers: ['last 2 versions'],
					cascade: false
			}))
			.pipe(concat('style.css'))
			.pipe(minifyCss())
			.pipe(gulp.dest(config.build))
			.pipe(browserSync.stream());

		gulp.src([
				config.assets+'/sass/litmus7.critical.scss',
			])
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(sass())
			.pipe(autoprefixer({
					browsers: ['last 2 versions'],
					cascade: false
			}))
			.pipe(concat('style.critical.css'))
			.pipe(minifyCss())
			.pipe(gulp.dest(config.build))
			.pipe(browserSync.stream());
	});

	gulp.task('libjs', function () {
		gulp.src([
			config.bower+'/jquery/dist/jquery.min.js',
			config.bower+'/jquery-lazy/jquery.lazy.min.js',
			config.bower+'/jquery-validation/dist/jquery.validate.js',
			config.bower+'/slick-carousel/slick/slick.js',
			config.bower+'/isotope-layout/dist/isotope.pkgd.min.js',
			config.bower+'/isotope-packery/packery-mode.pkgd.min.js',
			config.bower+'/bodymovin/build/player/lottie.min.js',
			config.bower+'/magnific-popup/dist/jquery.magnific-popup.min.js',
			config.bower+'/waypoints/lib/jquery.waypoints.js',
			config.bower+'/is_js/is.js',
			config.assets+'/js/vendor/clamp.min.js',
			config.assets+'/js/widget.carousel.js',
			config.assets+'/js/widget.grid.js',
			config.assets+'/js/widget.typewriter.js',
			config.assets+'/js/widget.contact.js',
			config.assets+'/js/widget.magnific-popup.js',
			config.assets+'/js/lazy.js',
			config.assets+'/js/menu.js',
			config.assets+'/js/animations.js',
			config.assets+'/js/scrolling.js',
			config.assets+'/js/phone.js',
			config.assets+'/js/skills.js',
			config.assets+'/js/services.js',
			config.assets+'/js/open-positions.js',
			config.assets+'/js/case-studies.js'
		])
			.pipe(uglify())
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(concat('core.js'))
			.pipe(gulp.dest(config.build+'/static/js/'))
			.pipe(browserSync.stream());
	});

	gulp.task('fonts', function () {
		var source = [config.bower + "/slick-carousel/slick/fonts/*"];
		var destination = config.build + "/static/fonts";

		gulp.src(source)
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(gulp.dest(destination));
	});

	gulp.task('images', function () {
		var source = [
			config.assets + "/img/**/*",
			config.bower + "/slick-carousel/slick/ajax-loader.gif"
		];
		var destination = config.build + "/static/img";

		gulp.src(source)
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(gulp.dest(destination));
	});

	gulp.task('json', function () {
		var source = [
			config.assets + "/json/**/*",
		];
		var destination = config.build + "/static/json";

		gulp.src(source)
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(gulp.dest(destination));
	});

	gulp.task('watch', function() {
		browserSync.init({
			proxy: "localhost:8888"
		});

		gulp.watch(config.assets+'/img/**/*.*', ['images']);
		gulp.watch(config.assets+'/json/**/*.*', ['json']);
		gulp.watch(config.assets+'/sass/**/*.scss', ['sass']);
		gulp.watch(config.assets+'/js/**/*.js', ['libjs']);
		gulp.watch(config.build+'/**/*.php', ['fonts', 'images']).on('change', browserSync.reload);
		gulp.watch(config.build+'/**/*.twig', ['fonts', 'images']).on('change', browserSync.reload);
	});

	gulp.task('default', ['watch']);

	gulp.task('build', ['sass', 'fonts', 'images', 'libjs']);
	
})();
