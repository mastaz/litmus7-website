<?php
/**
 * Template Name: Open Positions
 */

$post = Timber::query_post();
$positions_from_api = file_get_contents("http://hire.litmus7.com/api/v1/job/active-jobs");
$positions_from_api = json_decode($positions_from_api);
//$context['positions'] = $content->jobs;

$positions = array();
foreach ($positions_from_api as &$wrapper) {
  foreach ($wrapper as &$position) {
    $positions[] = array(
      'id' => $position->id,
      'position' => $position->title,
      'location' => $position->job_location,
      'description' => $position->description
    );
  }
}

$context = Timber::get_context();
$context['post'] = $post;
$context['positions'] = $positions;
Timber::render('templates/template.open_positions.twig', $context);