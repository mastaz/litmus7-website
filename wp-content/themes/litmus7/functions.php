<?php

include( 'functions.fields.php' );

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

function fb_move_admin_bar() {
	echo '
	<style type="text/css">
	body {
		margin-top: -28px;
		padding-bottom: 28px;
	}

	body.admin-bar #wphead {
		padding-top: 0;
	}

	body.admin-bar #footer {
		padding-bottom: 28px;
	}

	#wpadminbar {
		position: fixed !important;
		top: auto !important;
		bottom: 0;
	}
	
	#wpadminbar .quicklinks .menupop ul {
		bottom: 28px;
	}
	</style>';
}

Timber::$dirname = array('templates', 'views');

class Litmus7Site extends TimberSite {

	function __construct() {
		load_theme_textdomain('litmus7', get_template_directory() . '/languages');
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'custom-logo' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_filter( 'body_class', array( $this, 'rename_template_body_class' ) );
		add_filter( 'upload_mimes', array( $this, 'cc_mime_types') );
		add_filter( 'wpseo_opengraph_title', array( $this, 'change_title') );
		add_filter( 'wpseo_opengraph_desc', array( $this, 'change_desc') );
		add_filter( 'wpseo_opengraph_url', array( $this, 'change_url') );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'register_menus' ) );
		add_action( 'init', array( $this, 'register_settings' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'customize_register', array( $this, 'register_customizer' ) );
		add_action( 'wpseo_opengraph', array( $this, 'change_yoast_seo_og_meta' ) );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function register_menus() {
		$locations = array(
			'headerMenu' => 'Header',
			'footeMenu' => 'Footer',
		);
		register_nav_menus( $locations );
	}

	function register_settings() {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> 'Litmus7 Settings',
				'menu_title'	=> 'Litmus7 Settings',
				'menu_slug' 	=> 'litmus7-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false
			));
		}
	}

	function register_scripts() {
		wp_enqueue_script( 'litmus7-all', get_template_directory_uri() . '/static/js/core.js', array(), '*2018-10-15', true );
	}

	function register_customizer( $wp_customize ) {	
	}

	function rename_template_body_class( $classes ) {
		foreach ( $classes as $k =>  $v ) {
			if ( strpos( $v, 'page-template-template' ) !== false )
				$classes[ $k ] = str_replace( "page-template-", "", $v );
		}
		return $classes;
	}

	function add_to_context( $context ) {
		$context['headerMenu'] = new TimberMenu('headerMenu');
		$context['footerMenu'] = new TimberMenu('footerMenu');
		// $context['options'] = get_fields('option');
		$context['version'] = "*2018-10-15";
		$context['site'] = $this;
		return $context;
	}
 
	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		return $twig;
	}

	function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	function change_title($title) {
		if (get_page_template_slug(get_the_ID()) == "template.open_positions.php") {
			if (isset($_GET['position'])) {
				$current_position = $_GET['position'];
				$positions_from_api = file_get_contents("http://hire.litmus7.com/api/v1/job/active-jobs");
				$positions_from_api = json_decode($positions_from_api);

				foreach ($positions_from_api as &$wrapper) {
					foreach ($wrapper as &$position) {
						if ($current_position == sanitize_title($position->title)) {
							$title = __( $position->title . " - Join Litmus7 -  We're hiring!" );
							return $title;
						}
					}
				}
			}
		}

		return $title;
	}

	function change_desc($desc) {	
		if (get_page_template_slug(get_the_ID()) == "template.open_positions.php") {
			if (isset($_GET['position'])) {
				$current_position = $_GET['position'];
				$positions_from_api = file_get_contents("http://hire.litmus7.com/api/v1/job/active-jobs");
				$positions_from_api = json_decode($positions_from_api);

				foreach ($positions_from_api as &$wrapper) {
					foreach ($wrapper as &$position) {
						if ($current_position == sanitize_title($position->title)) {
							$desc = __("Litmus7 is looking for " . $position->title . " to join our team. We Are Artists, Leaders and Problem Solvers. Let's disrupt eCommerce together.");
							return $desc;
						}
					}
				}
			}
		}

		return $desc;
	}

	function change_url($url) {	
		if (get_page_template_slug(get_the_ID()) == "template.open_positions.php") {
			if (isset($_GET['position'])) {
				$current_position = $_GET['position'];
				$positions_from_api = file_get_contents("http://hire.litmus7.com/api/v1/job/active-jobs");
				$positions_from_api = json_decode($positions_from_api);

				foreach ($positions_from_api as &$wrapper) {
					foreach ($wrapper as &$position) {
						if ($current_position == sanitize_title($position->title)) {
							return $_SERVER['HTTP_REFERER'];
						}
					}
				}
			}
		}

		return $url;
	}
}

new Litmus7Site();
