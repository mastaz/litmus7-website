<?php
/**
 * Template Name: Services
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.services.twig', $context);