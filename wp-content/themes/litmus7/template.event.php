<?php
/**
 * Template Name: Event
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.event.twig', $context);
