<?php
/**
 * Template Name: Contact
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.contact.twig', $context);
