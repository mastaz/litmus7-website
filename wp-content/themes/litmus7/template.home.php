<?php
/**
 * Template Name: Homepage
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.home.twig', $context);
