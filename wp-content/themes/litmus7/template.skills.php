<?php
/**
 * Template Name: Skills
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
$context['filters'] = Timber::get_terms('category');
Timber::render('templates/template.skills.twig', $context);