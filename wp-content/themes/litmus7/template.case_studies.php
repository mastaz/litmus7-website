<?php
/**
 * Template Name: Case Studies
 */

$query_case_studies = array(
  'post_type' => 'case_studies',
  'posts_per_page' => -1,
  'orderby' => 'date'
);

$data = Timber::get_context();
$data['post'] = Timber::query_post();
$data['posts'] = Timber::get_posts($query_case_studies);
$data['filters'] = Timber::get_terms('practice');
Timber::render('templates/template.case_studies.twig', $data);