<?php
/**
 * Template Name: Default
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.default.twig', $context);
