<?php
/**
 * Template Name: Case Study
 */

$context = Timber::get_context();
$context['post'] = Timber::query_post();
Timber::render('templates/template.case_study.twig', $context);