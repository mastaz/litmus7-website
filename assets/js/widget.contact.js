(function () {
  "use strict";

  $(document).ready(function () {
    var $result;

    $result = $("#contact-form").validate({
      rules: {
        "first-name": "required",
        "last-name": "required",
        "email": {
          required: true,
          email: true
        },
        "phone": {
          required: true
        },
        "message": {
          required: true,
          minlength: 5
        }
      },
      messages: {
        "first-name": "Please type your first name",
        "last-name": "Please type your last name",
        "email": "Please type a valid email",
        "phone": "Please type your phone number",
        "message": ""
      }
    });

    $("#contact-form").submit(function (e) {
      var $form;
      var $result;

      $form = $(this);

      // Disable the postback
      e.preventDefault();

      // Validate form
      $result = $form.validate();

      if ($result.errorList.length === 0) {
        $.ajax({
          type: $form.attr("method"),
          url: $form.attr("action"),
          data: $form.serialize(),
          success: function (data) {
            if (data.status == "success") {
              $(".contact").addClass("--submitted");
              $(".--contact").addClass("--submitted");

              // Clear the form
              $("input, textarea").val("");
            }
          },
          error: function (event, jqxhr, settings, thrownError) {
            $(".contact").addClass("--error");
          }
        });
      }
    });
  });
})();