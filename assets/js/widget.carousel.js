(function () {
  "use strict";

  $(document).ready(function () {
    var CAROUSEL_CONFIG = {}

    CAROUSEL_CONFIG.DEFAULT = {
      mobileFirst: true,
      arrows: false,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 1180,
          settings: {
            centerMode: true,
            centerPadding: '450px',
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    CAROUSEL_CONFIG.MOBILE = {
      mobileFirst: true,
      arrows: false,
      dots: true,
      centerMode: true,
      centerPadding: '35px',
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            centerPadding: '30px',
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 1180,
          settings: "unslick"
        }
      ]
    };

    CAROUSEL_CONFIG.GALLERY = {
      mobileFirst: true,
      arrows: false,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 1180,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    $(".carousel").each(function(index, carousel) {
      var config;
      var dataConfig = $(carousel).attr("data-config");

      switch(dataConfig) {
        case "default": {
          config = CAROUSEL_CONFIG.DEFAULT;
          break;
        }
        case "mobile": {
          config = CAROUSEL_CONFIG.MOBILE;
          break;
        }
        case "gallery": {
          config = CAROUSEL_CONFIG.GALLERY;
          break;
        }
        default: {
          config = JSON.parse(dataConfig);
          break;
        }
      }

      $(carousel).slick(config);
    });

    // $('.carousel-mobile').slick({
    //   mobileFirst: true,
    //   arrows: false,
    //   dots: true,
    //   centerMode: true,
    //   centerPadding: '35px',
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   responsive: [
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         centerMode: true,
    //         centerPadding: '30px',
    //         slidesToShow: 2,
    //         slidesToScroll: 1
    //       }
    //     }, {
    //       breakpoint: 1180,
    //       settings: "unslick"
    //     }
    //   ]
    // });

    // $('.carousel').slick({
    //   mobileFirst: true,
    //   arrows: false,
    //   dots: true,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   responsive: [
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //       }
    //     }, {
    //       breakpoint: 1180,
    //       settings: {
    //         centerMode: true,
    //         centerPadding: '450px',
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //       }
    //     }
    //   ]
    // }); 

    // $('.image-carousel').slick({
    //   mobileFirst: true,
    //   arrows: false,
    //   dots: true,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   autoplay: true,
    //   autoplaySpeed: 2000,
    //   responsive: [
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //       }
    //     }, {
    //       breakpoint: 1180,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //       }
    //     }
    //   ]
    // });
  });
})();
