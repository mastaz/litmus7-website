(function () {
  "use strict";

  $(document).ready(function () {
    var THRESHOLD = parseInt($(".items").attr("data-threshold"));

    var $grid = $('.case-studies').isotope({
      itemSelector: '.item',
      layoutMode: 'packery'
    });

    // hide empty categories
    $(".filter").each(function(index, category) {
      var filterValue = $(category).find(".btn").attr("data-filter");

      if (filterValue === "*") {
        return;
      }

      var results = $(filterValue);
      if (results.length === 0) {
        $(category).hide();
      }
    })

    // bind filter button click
    $('.filters').on( 'click', 'button', function() {
      var filterValue = $(this).attr('data-filter');

      var $items = [];

      if (filterValue === "*") {
        filterValue = ".item";
      }

      $(".more-items").find(filterValue).each(function(index, item) {
        $items.push(item);
        $(item).remove();
      });

      $grid
        .append($items)
        .isotope( 'appended', $items )
        .isotope({
          filter: filterValue
        })
        .isotope('layout');

      if (filterValue === "*") {
        if ( $(".more-items").find(".item").length === 0 ) {
          $(".load-more").hide();
        } else {
          $(".load-more").show();
        }
      } else {
        $(".load-more").hide();
      }
    });

    // change is-checked class on buttons
    $('.filters').find('.filters-container').each( function( i, filters ) {
      var $filters = $(filters);
      $filters.on( 'click', 'button', function() {
        $filters.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
      });
    });
  });
})();