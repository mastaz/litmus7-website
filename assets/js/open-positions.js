(function () {
  "use strict";

  $(document).ready(function () {
    var THRESHOLD = 5;
    var $result;

    $(".js-vertical-tab-content").hide();
    $(".js-vertical-tab-content:first").show();

    /* if in tab mode */

    $(".js-vertical-tab").click(function(event) {
      // event.preventDefault();

      $(".js-vertical-tab-content").hide();
      var activeTab = $(this).attr("rel");
      $("#"+activeTab).show();

      $(".js-vertical-tab").removeClass("is-active");
      $(this).addClass("is-active");

      $(".js-vertical-tab-accordion-heading").removeClass("is-active");
      $(".js-vertical-tab-accordion-heading[rel^='"+activeTab+"']").addClass("is-active");

      var position = $(this).position();
      $( ".vertical-tab-content-container" ).css( "margin-top", position.top + "px" );

      // TODO: update - 
      // FACEBOOK:
      // 1. <meta property="og:url" content="XXXXXX">
      // 2. <meta property="og:description" content="YYYYYY">
      // 3. <meta property="og:title" content="ZZZZZZ">
      // TWITTER: (in the twig file)
      // <a 
      //   href="https://twitter.com/intent/tweet?source={{ function('the_permalink') }}#{{slug}}&text=ZZZZZZ" 
      //   target="_blank" 
      //   title="Tweet"
      // >
      // LINKEDIN: (in the twig file)
      // <a 
      //   href="http://www.linkedin.com/shareArticle?mini=true&url={{ function('the_permalink') }}#{{slug}}&title=ZZZZZZ&summary=YYYYYY&source={{ function('the_permalink') }}" 
      //   target="_blank" 
      //   title="Share on LinkedIn"
      // >
    });

    /* if in accordion mode */

    $(".js-vertical-tab-accordion-heading").click(function(event) {
      event.preventDefault();

      //$(".js-vertical-tab-content").hide();
      var accordion_activeTab = $(this).attr("rel");
      var target = $(this).attr("href");
      //$("#"+accordion_activeTab).show();

      
      if ($(this).find(".apply").text() === "Close") {
        $("#"+accordion_activeTab).hide();
        $(this).find(".apply").text("View Job Description");
      } else {
        $(".js-vertical-tab-accordion-heading").find(".apply").text("View Job Description");
        $(this).find(".apply").text("Close");
        $(".js-vertical-tab-content").hide();
        $("#"+accordion_activeTab).show();
      }
      
      $(".js-vertical-tab-accordion-heading").removeClass("is-active");
      $(this).addClass("is-active");

      $(".js-vertical-tab").removeClass("is-active");
      $(".js-vertical-tab[rel^='"+accordion_activeTab+"']").addClass("is-active");

      $('html, body').animate({
        scrollTop: $(target).offset().top - $('.header').height() - 45
      }, 1000);
    });

    // $(".vertical-tab-accordion-heading").find(".apply").click(function() {
    //   if ($(this).text() === "Close") {
    //     $(this).text("View Job Description");
    //   } else {
    //     $(this).text("Close");
    //   }
    // });

    $(".du-choosebtn")
    .removeClass("du-choosebtn")
    .addClass("btn-primary-inverted")
    .css("margin-top", "10px");


    $(".equal-opportunity .title").click(function() {
      $(".equal-opportunity").toggleClass("--collapsed");
    });

    /* init filters */

    var position = $(".--open-positions").find(".vertical-tabs-container").find(".position");
    position.each(function(index, position) {
      var data_location = position.dataset.location;

      if ($("select.locations-dropdown").find("option[value='" + data_location + "']").length === 0) {
        $(".dropdown").find("select.locations-dropdown").append("<option value='" + data_location + "'>" + data_location + "</option>");
      }
    });

    function loadMore() {
      $(".--open-positions")
          .find(".position.--not-loaded")
          .slice(0, THRESHOLD)
          .each(function(index, position) {
            $(position).removeClass("--not-loaded");
        });
    }

    $("select.locations-dropdown, select.teams-dropdown").change(function() {
      var selectedLocation = $("select.locations-dropdown").val();
      var query = "";

      if (selectedLocation) {
        query += "[data-location='" + selectedLocation + "']"
      }

      if (!query) {
        $(".--open-positions")
          .find(".vertical-tabs-container")
          .find(".vertical-tab")
          .css("display", "block");

        $(".--open-positions")
          .find(".vertical-tabs-container")
          .find(".vertical-tab-content-wrapper")
          .css("display", "block");
      } else {
        $(".--open-positions")
          .find(".vertical-tabs-container")
          .find(".vertical-tab")
          .css("display", "none")
          .filter(query)
          .css("display", "block");
  
        $(".--open-positions")
          .find(".vertical-tabs-container")
          .find(".vertical-tab-content-wrapper")
          .css("display", "none")
          .filter(query)
          .css("display", "block");
      }

      loadMore();
    }); 

    $('.open-popup')
      .on('click', function() {
        var id = this.dataset.id;
        var position = this.dataset.position;
        var location = this.dataset.location;
        var target = $(this).attr("href");
        $(target).find("#position").text(position);
        $(target).find("#location").text(location);
        $(target).find("#input-id").val(id);
        $(target).find("#input-position").val(position);
        $(target).find("#input-location").val(location);

        $(this)
          .magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#first-name',

            // When elemened is focused, some mobile browsers in some cases zoom in
            // It looks not nice, so we disable it:
            callbacks: {
              beforeOpen: function() {
                if($(window).width() < 700) {
                  this.st.focus = false;
                } else {
                  this.st.focus = '#first-name';
                }
              }
            }
          })
          .magnificPopup('open');
    });

    if ($("body").hasClass('template-open_positions')) {
      $(window).scroll(function(){
        var position = $('.load-more').offset();
        var scroll = window.scrollY; 
        var load = position.top - scroll;
        
        if (load <= 500) {
          loadMore();
        };
      });
    };

    $result = $("#apply-form").validate({
      rules: {
        "first-name": "required",
        "last-name": "required",
        "email": {
          required: true,
          email: true
        },
        "phone": {
          required: true
        },
        "portfolio": {
          required: true,
          url: true
        },
        "info": {
          required: true,
          minlength: 5
        }
      },
      messages: {
        "first-name": "Please type your first name",
        "last-name": "Please type your last name",
        "email": "Please type a valid email",
        "phone": "Please type your phone number",
        "portfolio": "Please type a valid URL",
        "info": "Please tell us about yourself"
      }
    });

    $("#apply-form").submit(function (e) {
      var $form;
      var $result;

      $form = $(this);

      // Disable the postback
      e.preventDefault();

      // Validate form
      $result = $form.validate();

      if ($result.errorList.length === 0) {
        $.ajax({
          type: $form.attr("method"),
          url: $form.attr("action"),
          data: $form.serialize(),
          success: function (data) {
            if (data.status == "success") {
              $("#apply").addClass("--submitted");

              // Clear the form
              $("input, textarea").val("");
            }
          },
          error: function (event, jqxhr, settings, thrownError) {
            $("#apply").addClass("--error");
          }
        });
      }
    });

    if (window.location.hostname === "litmus7.com") {
      $("#input-environment").val("production");
    } else {
      $("#input-environment").val("staging");
    }

    /* If there's a hash, select the corresponded tab  */
    var url = new URL(document.location.href);
    var hash = "#"+url.searchParams.get("position");
    var $tab = $("a[href='"+hash+"']");

    if ($tab.length > 0) {
      $($tab[0]).trigger("click");
    }
    /* =============================================== */
  });
})();