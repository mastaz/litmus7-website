Dropzone.options.resume = {
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 2, // MB
  dictDefaultMessage:"Upload Attachment (PDF, Doc, Etc.)",
  //acceptedFiles: "pdf, doc, docx",
  accept: function(file, done) {
    done();
  }
};