// external js: isotope.pkgd.js
(function () {
  "use strict";

  $(document).ready(function () {
    // init Isotope
    var $grid = $('.grid').isotope({
      itemSelector: '.box',
      layoutMode: 'packery'
    });

    // hide empty categories
    $(".filter").each(function(index, category) {
      var filterValue = $(category).find(".btn").attr("data-filter");

      if (filterValue === "*") {
        return;
      }

      var results = $(filterValue);
      if (results.length === 0) {
        $(category).hide();
      }
    })

    // bind filter button click
    $('.filters').on( 'click', 'button', function() {
      var filterValue = $(this).attr('data-filter');

      var $items = [];

      if (filterValue === "*") {
        filterValue = ".box";
      }

      $(".more-boxes").find(filterValue).each(function(index, box) {
        $items.push(box);
        $(box).remove();
      });

      $grid
        .append($items)
        .isotope( 'appended', $items )
        .isotope({
          filter: filterValue
        })
        .isotope('layout');

      if (filterValue === "*") {
        if ( $(".more-boxes").find(".box").length === 0 ) {
          $(".load-more").hide();
        } else {
          $(".load-more").show();
        }
      } else {
        $(".load-more").hide();
      }
    });

    // change is-checked class on buttons
    $('.filters').find('.filters-container').each( function( i, filters ) {
      var $filters = $(filters);
      $filters.on( 'click', 'button', function() {
        $filters.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
      });
    });
  });

})();