(function () {
    "use strict";

    function init() {
        $('.animated').waypoint(function() {
            var $element = $($(this)[0].element);
            var animation = $element.attr('data-animation');
            $element.addClass(animation);
        });

        $('.animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $(this).removeClass('animated');
        });

        // ==========================================

        $(".svg-animation").each(function(index, svg) {
            lottie.loadAnimation ({
                container: svg, // the dom element that will contain the animation
                renderer: 'svg',
                loop: true,
                autoplay: true,
                path: svg.dataset.animation // the path to the animation json
            });
        });
    }

    init();
})();