(function () {
  "use strict";

  $(document).ready(function () {
    $('.open-popup').magnificPopup({
      type: 'inline', 
      preloader: false
    });
  });
})();