(function () {
  "use strict";

  $(document).ready(function () {
    var close = function(element) {
      var $service_link = $(element).closest(".service-item").find(".service-link");
      var $overlay = $(element).closest(".section__content").find(".overlay");
      $service_link.removeClass("--open");
      $overlay.removeClass("--open");
    }

    $(".service-number").click(function() {
      var $service_link = $(this).closest(".service-item").find(".service-link");
      var $overlay = $(this).closest(".section__content").find(".overlay");
      $service_link.toggleClass("--open");
      $overlay.toggleClass("--open");
    });

    $(".service-link__close").click(function() {
      close(this);
    });

    $(".service-link").find("a").click(function() {
      var that = this;

      setTimeout(function() {
        close(that);
      }, 1000);
    });

    $(".item").find(".text").each(function(index, text) {
      $clamp(text, {clamp: 6});
    });

    $(".show-more").click(function() {
      var $text = $(this).closest(".item").find(".text");

      $clamp($text[0], {clamp: 99999});
      $(this).toggleClass("--expand");

      if ($(this).is(".--expand")) { 
        $(this).text('SHOW LESS');
      } else {
        $(this).text('SHOW MORE');
        $clamp($text[0], {clamp: 6});
      }
    });
  });
})();