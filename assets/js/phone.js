$(function() {
    if (is.desktop()) {
        var list = $("a[href^='tel:']");
        $.each(list, function(index, value) {
            var $phone = $(value);
            $phone.parent().html($phone.text());
        })
    };
});