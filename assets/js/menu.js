(function () {
  "use strict";

  $(document).ready(function () {
    $(".mobile-menu-button").click(function() {
      $(".header-menu").toggleClass("--open");
  
      if ($(".header-menu").is(".--open")) {
        $("body").css("overflow", "hidden");
      } else {
        $("body").css("overflow", "auto");
      }
    });

    $(".header-menu").find("a").click(function() {
      if ($(".header-menu").is(".--open")) {
        $(".header-menu").removeClass("--open");
      }
    });
  });
})();
