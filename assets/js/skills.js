(function () {
  "use strict";

  $(document).ready(function () {
    var $grid = $('.grid').isotope('layout');

    $(".box-button")
      .click(function () {
        var $box = $(this).closest(".box");

        $box.toggleClass("--open");
        if ($box.hasClass("--open")) {
          $box
            .find(".box-front")
            .hide();
          $box
            .find(".box-back")
            .fadeIn(250, function () {
              $grid.isotope('layout');
            });
        } else {
          $box
            .find(".box-back")
            .hide();
          $box
            .find(".box-front")
            .fadeIn(250, function () {
              $grid.isotope('layout');
            });
        }
      });
    if ($("body").hasClass('template-skills')) {
      $(window).scroll(function(){
        var position = $('.load-more').offset();
        var scroll = window.scrollY; 
        var load = position.top - scroll;
  
        if (load <= 700) {
          var threshold = parseInt($(".grid.boxes").attr("data-threshold"));
          var $items = [];
    
          $(".more-boxes")
            .find(".box")
            .each(function (index, box) {
              if (index >= 0 && index < threshold * 2) {
                $items.push(box);
                $(box).remove();
              }
            });
    
          $grid
            .append($items)
            .isotope('appended', $items)
            .isotope('layout');
        }
      })  
    };
  });
})();
